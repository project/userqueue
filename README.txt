User Queue -- a module similar to nodequeue but for users.

This module allows you to create ordered queues of users and display them as blocks.

The interface and design is similar to nodequeue. Just enable the module and give out "administer user queues" permissions as necessary. The forms to add, edit, and popular user queues will appear in the "User management" section of the admin area.